# datadump

Export des données RESIF

## Workflow

Description du workflow dans notre wiki : https://wiki.osug.fr/!isterre-geodata/resif/systemes/services/datadump

## Utilisation

Le script lance 4 jobs en parallele pour effectuer ses tâches.

Les configurations se font par variable d'environnement :

  * `RESIFDD_WORKDIR` : *obligatoire* le répertoire de travail dans lequel le script prépare ses paquets avant expédition, écrit son rapport, etc.
  * `RESIFDD_DATADIR` : *obligatoire* le répertoire où le script pourra trouver les points de montage SUMMER `validated_seismic_metadata` et `validated_seismic_data`
  * `RESIFDD_CONTINUE_FROM_FILE` : la valeur est un fichier de rapport précédent à partir duquel le script pourra reprendre le travail là où il l'a laissé. Si le rapport mentionne des erreurs de transfert, le script réessayera
  * `RESIFDD_START_AT` : permet d'indiquer une année à partir de laquelle reprendre le transfert. Tous les éléments appartenant à une année inférieur sont ignorés
  * `RESIFDD_KEYFILE` : si cette variable indique le chemin d'un fichier valide, alors il sera utilisé pour transférer les données correspondantes aux clés listées dans le fichier.
  * `GITLAB_TOKEN` : si cette variable est fournie, alors le script va récupérer tous les projets GIT pour les sauvegarder. Ce token doit avoir les privilègres `read_api` et `read_repository` sur tout le groupre OSUG/RESIF.
  
Et pour la connexion à la base de donnée, il faut renseigner :

  * `PGHOST`: le serveur postgres (par défaut `localhost`)
  * `PGPORT` : le port de connexion (par défaut 5432)
  * `PGUSER` : l'utilisateur pour la connexion. Il doit avoir les droits SELECT sur la table rall
  * `PGDATABASE` : la base de donnée d'inventaire
  * `PGPASSWORD` : le mot de passe postgres

## Exemples
### Lancer tout le dump

``` shell
GITLAB_TOKEN=plop RESIFDD_WORKDIR=/osug-dc/resif RESIFDD_DATATIR=/scratch/resifdumper resifdatadump
```

### Options particulières de l'outil

Lancer la sauvegarde d'une liste de stations :
``` shell
RESIFDD_DATADIR=/osug-dc/resif RESIFDD_WORKDIR=/scratch/resif_datadump src/resifdatadump 2011/RA/NCAD 2012/MT/THE
```

Sauvegarder les métadonnées
``` shell
RESIFDD_DATADIR=/osug-dc/resif RESIFDD_WORKDIR=/scratch/resif_datadump src/resifdatadump validated_seismic_metadata
```

On peut générer un fichier de clés avec le script python `src/scan_dupms.py` qui contrôle la présence des dumps distants
