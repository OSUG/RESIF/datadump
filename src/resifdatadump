#!/bin/bash
#
# Auteur: Jonathan Schaeffer <jonathan.schaeffer@univ-grenoble-alpes.fr>
# This script dumps precious RESIF data and metadata to an IRODS server
#

# This line tells to redirect all outputs to logger and stdout
exec 1> >(logger -s -t "$(basename $0)") 2>&1

set -a
####################
#
# Zabbix sender
#
###################

zabbix_err(){
    zabbix_sender -k resifdatadump.failed -s "$(hostname -f)" -o "${@}" -z monitoring.osug.fr
}

zabbix_ok(){
    zabbix_sender -k resifdatadump.ok -s "$(hostname -f)" -o "${@}" -z monitoring.osug.fr
}

# Format a report line. Parameters is the list of data to append to the report :
# "2019_FR_PLOP" "123456" '2015-07-15T06:51:02' duration throughput "feaaaaaa" "OK"
format_report(){
    IFS='_' read -r -a YNS <<< $1
    if [[ ${#YNS[@]} -eq 3 ]]; then
        Y=${YNS[0]}
        N=${YNS[1]}
        S=${YNS[2]}
    else
        Y=${YNS[0]}
        N="-"
        S="-"
    fi
    SIZE=$2
    DATE=$3
    DURATION=$4
    THROUGHPUT=$5
    COMMENT=${*:6}
    printf " %6s | %9s | %9s | %13s | %16s | %6s | %6s | %s" "$Y" "$N" "$S" "$SIZE" "$DATE" "$DURATION" "$THROUGHPUT" "${COMMENT}"
    echo
}

# Push data to irods as a staging file
# Argument is the distant directory to push to.
# If something goes wrong, roll back
# If everything goes fine, then validate staging data
irods_push(){
    KEY=$1
    LOCAL_SHA=$2
    SIZE=$(stat -c %s $RESIFDD_WORKDIR/$KEY.tar)
    SIZEMB=$((SIZE/1024/1024))

    for n in $(seq 1 5); do [ $n -gt 1 ] && sleep 10 ; imkdir -p $KEY && s=0 && break || s=$?; done
    if [[ $s -ne 0 ]]; then
        echo "[$KEY] Error 002 creating remote directory. Manual action has to be taken."
        format_report $KEY $SIZEMB "$(date +%Y-%m-%dT%H:%M:%S)" - - 'Error 010. imkdir failed' >> $LOCAL_REPORT
        zabbix_err "${KEY}:Error 002"
        return 1
    fi
    echo "[$KEY] Cleaning old staging.tar if exists"
    irm -f $KEY/staging.tar > /dev/null 2>&1
    echo "[$KEY] Sending data to iRODS ($SIZEMB MB)"

    COMMAND="iput --retries 5 -T -f -X $RESIFDD_WORKDIR/${KEY}.restart $RESIFDD_WORKDIR/${KEY}.tar $KEY/staging.tar"
    start=$(date +%s)
    eval $COMMAND
    IPUTRC=$?
    if [[ $IPUTRC -ne 0 ]]; then
        echo "[$KEY] Error 011 sending file to irods. The command was: $COMMAND"
        format_report $KEY $SIZEMB "$(date +%Y-%m-%dT%H:%M:%S --date=@$start)" - - 'Error 011. Transfer cancelled' >> $LOCAL_REPORT
        # Roll back
        irods_rollback $KEY
        # Alert to zabbix
        zabbix_err "${KEY}:Error 011"
    else
        duration=$(($(date +%s)-start))
        throughput=$((SIZEMB / duration ))
    # Check integrity.
    # We do it separately from transfer because for big files it can be very long, and the network can cut
        irods_sha=$(ichksum $KEY/staging.tar | awk -F':' '/sha2:/ {print $2; exit;}')
        if [[ "$LOCAL_SHA" != "$irods_sha" ]]; then
            echo "[$KEY] Error 012 distant file is corrupted (localsha $LOCAL_SHA irodssha $irods_sha). Rollback"
            irods_rollback $KEY
            zabbix_err "${KEY}:Error 012"
        fi
        echo "[$KEY] staging.tar data sent, let's commit everything on irods server"
        format_report $KEY $SIZEMB "$(date +%Y-%m-%dT%H:%M:%S --date=@$start)" $duration $throughput 'OK' >> $LOCAL_REPORT
        irods_commit $KEY
    fi
    zabbix_ok "$KEY|${SIZEMB}MB|${duration}s|${throughput}MB/s"
    # Send report to irods. Do some locking here
    (
      flock -e 200
      iput -f $LOCAL_REPORT $IRODS_REPORT
    ) 200>/$RESIFDD_WORKDIR/report.lock
}

# In case of any problem, this function rolls every distant file operation bask
# 1. tries to revover latest.tar from previous.tar
# 2. tries to recover previous.tar from previous_to_delete.tar
# Argument is the distant directory to work with
irods_rollback(){
    KEY=$1
    irm -f ${KEY}/staging.tar
    echo "[$KEY] Rollback : try to recover latest.tar from previous"
    ils ${KEY}/previous.tar 2>/dev/null && (
        if ! imv ${KEY}/previous.tar ${KEY}/latest.tar; then
            echo "[$KEY] Error 008 recovering latest.tar Exit 1"
            return 1
        fi
    )
    echo "[$KEY] Rollback : OK"
    echo "[$KEY] Rollback : try to recover previous.tar from previous_to_delete"
    ils ${KEY}/previous_to_delete.tar 2>/dev/null && (
        if ! imv ${KEY}/previous_to_delete.tar ${KEY}/previous.tar; then
            echo "[$KEY] Error 009 recovering previous.tar Exit 1"
            return 1
        fi
    )
    echo "[$KEY] Rollback : OK"
    return 0
}

# When the transfer to irods is successfull, we can move file arounds remotely :
# 1. $KEY/previous.tar move to trash
# 2. $KEY/latest.tar move to $KEY/previous.tar
# 3. $KEY/staging.tar move to $KEY/latest.tar
irods_commit(){
    KEY=$1
    echo "[$KEY] If previous.tar exists, move it around but keep it safe"
    ils $KEY/previous.tar 2>/dev/null && (
        if ! imv $KEY/previous.tar $KEY/previous_to_delete.tar; then
            echo "[$KEY] Error 003 moving previous.tar around. Corrective action has to be taken manualy"
            zabbix_err "${KEY}:Error 003"
            return 1
        fi
    )

    echo "[$KEY] If latest.tar exists, move it to previous.tar"
    ils $KEY/latest.tar 2>/dev/null && (
        if ! imv $KEY/latest.tar $KEY/previous.tar; then
            echo "[$KEY] Error 004 moving latest.tar to previous.tar. Corrective action has to be taken manualy"
            zabbix_err "${KEY}:Error 004"
            return 1
        fi
    )
    echo "[$KEY] Validate staging.tar by moving it to latest.tar"
    if ! imv $KEY/staging.tar $KEY/latest.tar; then
        echo "[$KEY] Error 005 moving statging.tar to latest.tar. Corrective action has to be taken manualy"
        zabbix_err "${KEY}:Error 005"
        return 1
    fi
    ils $KEY/previous_to_delete.tar 2>/dev/null && irm -f $KEY/previous_to_delete.tar
}

# Choose if the data should be processed or skipped
# Pack the data and send it to iRODS
# Argument is the path (abs or relative) to the data
# Optional second argument can be the parallel slot number as given by {%}
pack_and_send() {
    [ $# -eq 0 ] && echo "[pack_and_send] Need a path for data to send" && return 1
    # Parse path to get year, station and network
    dir=$1
    # First test if path exists, else return
    if [[ ! -d $dir ]]; then
        return 1
    fi
    IFS='/' read -r -a YNS <<< $dir
    [ ${#YNS[@]} -lt 4 ] && echo "[pack_and_send] Path $dir is not complete (${#YNS[@]} levels) " && return 2
    YEAR=${YNS[-3]}
    NETWORK=${YNS[-2]}
    STATION=${YNS[-1]}
    KEY=${YEAR}_${NETWORK}_${STATION}
    push=1
    echo "[$KEY] Starting job $2"
    # Test if in recovery mode, we should send or not
    if [[ -r $RECOVERY_FILE  ]]; then
        if grep -q -E ".*$YEAR.*$NETWORK.*$STATION.*( OK | Skipped ).*" $RECOVERY_FILE; then
            echo "[$KEY] Found OK or skipped in ${RECOVERY_FILE}. Skipping"
            format_report $KEY "-" "$(date +%Y-%m-%dT%H:%M:%S)" "-" "-" "Skipped" >> $LOCAL_REPORT
        fi
    fi
    # Connect to inventory database to check for recent files.
    echo "[$KEY] Creating tar on $RESIFDD_WORKDIR/$KEY.tar"
    tar_cmd="tar cf $RESIFDD_WORKDIR/$KEY.tar -C ${dir%"$YEAR/$NETWORK/$STATION"} ${YEAR}/${NETWORK}/${STATION}"
    echo "[$KEY] $tar_cmd"
    if ! eval $tar_cmd; then
        # Something went wrong creating archive. Exit
        echo "[$KEY] Error 007 creating tar"
        # Send key to zabbix_err
        zabbix_err "$KEY:Error 007"
    fi
    local_sha=$(sha256sum $RESIFDD_WORKDIR/$KEY.tar | awk '{print $1}' | xxd -r -p | base64)
    # Check if file exists on irods server
    if ! (ils -L $KEY/latest.tar > /dev/null 2>&1) ; then
        echo "[$KEY] latest.tar already exists on iRODS server. Let's compare hashes"
        irods_sha=$(ichksum $KEY/latest.tar | awk -F':' '/sha2:/ {print $2; exit;}')
        echo "[$KEY] local checksum: $local_sha"
        echo "[$KEY] irods checksum: $irods_sha"
        # If the hashes differs, then move distant file and push this one
        if [[ "$local_sha" = "$irods_sha" ]]; then
            echo "[$KEY] The archive on irods is the same as our version. Skipping."
            format_report $KEY "-" "$(date +%Y-%m-%dT%H:%M:%S)" "-" "-" "Skipped" >> $LOCAL_REPORT
            push=0
        fi
    fi
    # Send latest archive file to IRODS
    if [[ $push -eq 1 ]]; then
        echo "[$KEY] Pushing to irods"
        irods_push $KEY $local_sha
    fi
    rm $RESIFDD_WORKDIR/$KEY*

}

export -f pack_and_send   # Necessary for call with GNU parallel

####################
#
# Preliminary tests
#
####################

# Checking IRODS environment
if [[ ! -f ~/.irods/.irodsA ]] ; then
    echo "The irods scrambled password file is not present. Please run iinit and provide the password. Exit 1"
    exit 1
fi
# Test for working directory
if [[ ! -w $RESIFDD_WORKDIR ]] ; then
    echo "RESIFDD_WORKDIR \"$RESIFDD_WORKDIR\" not writable. Check permissions. Exit 1"
    exit 1
fi
# test the data directory where to dump everything from
if [[ -z $RESIFDD_DATADIR ]]; then
    echo "Variable RESIFDD_DATADIR must be set to the RESIF mountpoint. Exit 1"
    exit 1
fi
if [[ ! -d $RESIFDD_DATADIR ]]; then
    echo "Variable RESIFDD_DATADIR set to \"$RESIFDD_DATADIR\" must be a directory. Exit 1"
    exit 1
fi

# Test for starting date. This is a mandatory parameter now.
if [[ -z ${RESIFDD_SINCE} ]]; then
    echo "No date provided to start from. Please specify a date like RESIFDD_SINCE=2022-06-01"
    exit 1
else
    echo "Dumping every change since ${RESIFDD_SINCE}"
    # Try to interpret the date format right
    if ! from_date=$(date -d "${RESIFDD_SINCE}" +%Y-%m-%d); then
        echo "Date format not recognized. Please specify a date like RESIFDD_SINCE=2022-06-01"
        exit 1
    fi
fi
# Test for database access in order to check the changes
if ! psql -qtA -c "SELECT * from rall limit 1;"; then
    echo "Unable to connect to database. Please specify all necessary environment variables for connection to inventory database:"
    echo " - PGHOST=$PGHOST"
    echo " - PGPORT=$PGPORT"
    echo " - PGDATABASE=$PGDATABASE"
    echo " - PGUSER=$PGUSER"
    echo " - PGPASSWORD=XXXXXXXX"
    echo "Make sure connection to database works without interaction and access is granted to table rall."
    exit 1
fi

####################
#
# Option ContinueFrom
#
####################
if [[ -r ${RESIFDD_CONTINUE_FROM_FILE} ]]; then
    # Continue from previous report
    echo "Recovery file $RESIFDD_CONTINUE_FROM_FILE exists"
    cp $RESIFDD_CONTINUE_FROM_FILE $RESIFDD_WORKDIR/recovery.$$
    RECOVERY_FILE=$RESIFDD_WORKDIR/recovery.$$
    echo "Now using $RESIFDD_WORKDIR/recovery.$$ as recovery file"
else
    echo "No recovery file set"
fi

# Header for the report :
IRODS_REPORT=reports/$(date +%Y%m%d-%H%M).csv
LOCAL_REPORT=$RESIFDD_WORKDIR/report_$(date +%Y%m%d-%H%M).csv
format_report "Year_Network_Station" "Size(MB)" "Dumpdate" "Duration(s)" "Throughput(MB/s)" "Comment" > $LOCAL_REPORT
imkdir -p reports
iput -f $LOCAL_REPORT $IRODS_REPORT

##################
#
# Dump projects hosted at gricad-gitlab
#
##################

# Test if RESIFDD_GITLAB_TOKEN environment variable is provided
if [ -z ${RESIFDD_GITLAB_TOKEN+x} ]; then
    echo "RESIFDD_GITLAB_TOKEN is not set. We will NOT dump gitlab projects"
else
    KEY=git_projects
    start=$(date +%s)
    if [[ -r $RECOVERY_FILE ]] && grep -q -E ".*($KEY ).*( OK | Skipped ).*" $RECOVERY_FILE ; then
        format_report $KEY "-" "$(date +%Y-%m-%dT%H:%M:%S)" "-" "-" "Skipped" >> $LOCAL_REPORT
    else
        username="ccdump"
        # number of results per pages (max = 100)
        per_page=50
        url_repositories="https://gricad-gitlab.univ-grenoble-alpes.fr/api/v4/groups/192/projects?simple=true&include_subgroups=true&per_page=$per_page"
        # total number of projects
        total=$(curl -i -s --header "PRIVATE-TOKEN: $RESIFDD_GITLAB_TOKEN" "$url_repositories" | awk '/x-total:/ {printf "%.0f\n", $2}')
        # check if $total is a number:
        re_is_num='^[0-9]+$'
        if [[ "$total" =~ $re_is_num ]]; then
            # max page calculation
            max_page=$((total / per_page +1))
            for page in $(seq $max_page); do
                # get projects list
                project_list=$(curl -s --header "PRIVATE-TOKEN: $RESIFDD_GITLAB_TOKEN" $url_repositories"&page="$page | grep -o "\"http_url_to_repo\":[^ ,]\+" | xargs -n 1|sed -e 's/http_url_to_repo:https:\/\///')
                for url in ${project_list[@]}; do
                    # This pattern captures the project name (group 2) and the possible subdirectories (group 1).
                    pattern="^.*\/OSUG\/RESIF\/(.*\/)*(.+.git)$"
                    if [[ "$url" =~ $pattern ]]; then
                        destdir=$RESIFDD_WORKDIR/projects/${BASH_REMATCH[1]}
                        project=${BASH_REMATCH[2]}
                        mkdir -p $destdir
                        if [[ -d $destdir/$project ]]; then
                            echo "Pulling $url in $destdir/$project"
                            git -C $destdir/$project pull
                        else
                            echo "Cloning $url in $destdir/$project"
                            git clone -q --bare https://$username:$RESIFDD_GITLAB_TOKEN@$url $destdir/$project
                        fi
                    fi
                done
            done
            # Now compress all the project and send to irods
            tar cJf $RESIFDD_WORKDIR/${KEY}.tar.xz -C $RESIFDD_WORKDIR $RESIFDD_WORKDIR/projects
            SIZE=$(stat -c %s $RESIFDD_WORKDIR/$KEY.tar.xz)
            SIZEMB=$((SIZE/1024/1024))
            iput --retries 5 -T -f -X $RESIFDD_WORKDIR/${KEY}.restart $RESIFDD_WORKDIR/${KEY}.tar.xz .
            duration=$(($(date +%s)-start))
            throughput=$((SIZEMB / duration ))
            format_report $KEY $SIZEMB "$(date +%Y-%m-%dT%H:%M:%S --date=@$start)" $duration $throughput 'OK' >> $LOCAL_REPORT
            rm -rf  $RESIFDD_WORKDIR/projects $RESIFDD_WORKDIR/${KEY}.tar.xz
        else
            echo "ERROR: gitlab projects could not be dumped, maybe token or gitlab url is wrong ?"
        fi
    fi
fi


##################
#
# Dump Metadata
#
##################
KEY="validated-seismic-metadata"

if [[ -r $RECOVERY_FILE ]] && grep -q -E ".*($KEY ).*( OK | Skipped ).*" $RECOVERY_FILE ; then
    format_report $KEY "-" "$(date +%Y-%m-%dT%H:%M:%S)" "-" "-" "Skipped" >> $LOCAL_REPORT
else
    # Get the snapshot name for this month
    week_begin=$(date -d 'last sunday' '+%Y-%m-%d')
    SNAPSHOT_DIR=$(ls -d $RESIFDD_DATADIR/validated_seismic_metadata/.snapshot/weekly.${week_begin}*|tail -1)
    if [[ ! -d $SNAPSHOT_DIR ]]; then
        echo "Error 000 Snapshot directory $SNAPSHOT_DIR does not exist"
        exit 1
    fi

    echo "[$KEY] Starting dump from ${SNAPSHOT_DIR}"
    if ! tar cf  $RESIFDD_WORKDIR/$KEY.tar --exclude portalproducts -C $SNAPSHOT_DIR $SNAPSHOT_DIR; then
        echo "[$KEY] Error 001 while creating tar archive."
        zabbix_err "${KEY}:Error 001"
        exit 1
    fi
    local_sha=$(sha256sum $RESIFDD_WORKDIR/$KEY.tar | awk '{print $1}' | xxd -r -p | base64)
    irods_push $KEY $local_sha
    echo "[$KEY] Dump terminated :"
    ils -l $KEY
fi

####################
# List all directory that have new data
#
# From all row in rall where created_at between the RESIFDD_SINCE parameter and the beginning of this month
#
####################
target_directories=$(psql -qtAc "select distinct case when n.endtime='infinity' then n.network||'/'||r.year||'/'||r.station else n.network||n.start_year||'/'||r.year||'/'||r.station end from rall as r LEFT JOIN networks as n on  r.network_id=n.network_id where r.created_at between '$from_date' and '$(date +%Y-%m-01)';")

####################ddd
####################
#
# Start dumping validated data
#
####################

MONTH=$(date +%Y-%m)
SNAPSHOT_DIR=$(ls -d ${RESIFDD_DATADIR}/validated_seismic_data/.snapshot/monthly.${MONTH}*|tail -1)
if [[ ! -d $SNAPSHOT_DIR ]]; then
    echo "Error 006 Snapshot directory $SNAPSHOT_DIR does not exist"
    exit 1
fi

echo "Starting dump of validated data with 4 jobs"
echo "$target_directories" | awk -v dir=$SNAPSHOT_DIR '{print dir"/"$0}' | parallel --jobs 4 --max-args 1 pack_and_send {} {%}
echo "Dump of validated data done"

####################
#
# Start dumping cold data
#
####################

SNAPSHOT_DIR=$(ls -d ${RESIFDD_DATADIR}/cold_validated_seismic_data/.snapshot/weekly."$(date +%Y-%m-%d --date 'last sunday')"_*|tail -1)
if [[ ! -d $SNAPSHOT_DIR ]]; then
    echo "Error 006 Snapshot directory $SNAPSHOT_DIR does not exist"
    exit 1
fi
echo "Starting dump of cold data with 4 jobs"
echo "$target_directories" | awk -v dir=$SNAPSHOT_DIR '{ print dir"/"$0 }' | parallel --jobs 4 --max-args 1 pack_and_send {} {%}
echo "Dump of cold validated data done"

####################
#
# Start dumping node data
#
####################
echo "Starting dump of node data"
SNAPSHOT_DIR=$(ls -d ${RESIFDD_DATADIR}/validated_nodes_data/.snapshot/weekly."$(date +%Y-%m-%d --date 'last sunday')"_*|tail -1)
irsync -r $SNAPSHOT_DIR i:validated_nodes_data/
echo "Dump of node data done"
