# Ce script scanne l'archive distante IRODS
# Pour tout élément renvoyé (qui est de la donnée) :
# - si on trouve son existance dans validated_seismic_data alors on garde
# - sinon, on cherche dans cold_validated_seismic_data
# - sinon on supprime d'irods le répertoire

RESIF_DATA_DIR=/osug-dc/resif
ARCHIVE_DIRS="$RESIF_DATA_DIR/validated_seismic_data $RESIF_DATA_DIR/cold_validated_seismic_data"

IRODS_DATA=$(ils | awk -F'/' '/\/[0-9A-Z]+_[0-9A-Z]+_[0-9A-Z]+$/ {print $NF}')

for key in $IRODS_DATA; do
    data_path=$(echo $key|tr '_' '/')
    data_path_exists=0
    for dir in $ARCHIVE_DIRS; do
      if [[ -d $dir/$data_path ]]; then
          data_path_exists=1
          break
      fi
    done
    if [[ $data_path_exists -eq 0 ]]; then
        echo $data_path not found on local archive. What now ?
        echo irm $key ?
    fi
done
