#!/usr/bin/python3
"""
Ce script compare l'archive locale avec ce qui est sur irods.
Il compare seulement le nom des répertoires.
Si le répertoire existe en local et n'existe pas sur iRODS alors le script l'ajoute
à la liste des répertoires manquant
"""


import subprocess
import re
import glob,os.path
from collections import defaultdict

def get_irods_content():
  remote_data = defaultdict(lambda: defaultdict(dict))
  result = subprocess.run(['ils','-r', '-L'], stdout=subprocess.PIPE)

  # On prepare une structure de donnees (dict) :
  # remote_data = [
  #   key => [
  #          latest.tar   => [sha2 => '', size => ''] ,
  #          previous.tar => [sha2 => '', size => '']
  #          ]
  #   ], ...
  # ]

  # Test
  # root_dir='/tempZone/home/jschaeffer/'
  # Prod :
  root_dir='/cc-lyon/synchro/resif/'
  current_key = ''
  total_size = 0
  for line in result.stdout.decode('utf-8').split('\n'):
    if line.startswith(root_dir):
      current_key = line.replace(root_dir, '').replace('_','/').replace(':','')
    elif re.search('[0-9]{4}/[A-Z0-9]+/[A-Z0-9]+', current_key):
      if re.search('generic.*\.tar\s*$', line):
        #   sha2:g+RIFc7e6CVgkxpuucvj8GFpE7C8Vg0n4JdJmvUZtkE= generic /irods/Vault/archivage/resif/synchro/resif/2008_ZO_Y111/latest.tar
        if re.search('sha2:', line):
          remote_data[current_key][words[-1]]['sha2'] = re.split('\s+', line)[1].split(':')[1]
      elif re.search('\.tar\s*$', line) :
        # resif 0 Resif1;resifcache1 3985223680 2019-07-19.04:36 & latest.tar
        words = re.split(' +', line)
        remote_data[current_key][words[-1]]['size'] = words[4]
        total_size = total_size + int(words[4])

  print("Total iRODS storage used : "+str(total_size/(1024^3))+"GB")
  return remote_data

# Maintenant, on a l'état sur le serveur irods. Comparons avec notre dépôt :
# Chercher les tribples YYYY/NET/STATION dans /osug-dc/resif/validated_seismic_data
# Pour chacun, vérifier son existence dans remote_data
# S'il n'existe pas, on affiche un message et on garde la clé sous le coude.

def browse_local_data():
  filesDepth3 = glob.glob('/osug-dc/resif/validated_seismic_data/*/*/*')
  dirsDepth3 = filter(lambda f: os.path.isdir(f), filesDepth3)
  return list(dirsDepth3)


if __name__ == "__main__":
    dirs = browse_local_data()
    remote_data = get_irods_content()
    missing_keys = []
    for d in dirs:
        key = d.replace('/osug-dc/resif/validated_seismic_data/', '')
        if key in remote_data.keys():
            print(key+" => OK")
        else :
            missing_keys.append(key.replace('/','_'))
            print(key+" missing")

    print("List of missing keys (usable with RESIFDD_KEYFILE) : ")
    print('\n'.join(missing_keys))
