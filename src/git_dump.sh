#!/bin/bash

############# variables ################
# authentification username
username="ccdump"

# token for ccdump 
TOKEN="emN7szEu-hchxv-eYS6D"

# url prefix in children.json file
PREFIX="http_url_to_repo"

# number of results per pages (max = 100)
per_page=50

# les repertoires git de OSUG/RESIF/ se trouvent dans le fichier children.json
url_repositories="https://gricad-gitlab.univ-grenoble-alpes.fr/api/v4/groups/192/projects?simple=true&include_subgroups=true&per_page=$per_page"

############ script ##################
# total number of projects
total=$(curl -i -s --header "PRIVATE-TOKEN: $TOKEN" "$url_repositories" | awk '/x-total:/ {printf "%.0f\n", $2}') 
# max page calculation
max_page=$(($total / $per_page +1))

for page in `seq $max_page`; do
	#  project list creation
	project_list=$(curl -s --header "PRIVATE-TOKEN: $TOKEN" $url_repositories"&page="$page | grep -o "\"$PREFIX\":[^ ,]\+" | xargs -n 1|sed -e 's/http_url_to_repo:https:\/\///')
	for url in ${project_list[@]}; do
#		ygricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/ws-availability-k8s.git
	    pattern="^.*\/OSUG\/RESIF\/(.*\/){0,1}(.+.git)$"
		if [[ "$url" =~ $pattern ]]; then
			destdir=$RESIFDD_WORKDIR/projetcs/${BASH_REMATCH[1]}
			project=${BASH_REMATCH[2]}
			echo "Cloning $url in $destdir/$project"
			mkdir -p $destdir
        	git clone -q --bare https://$username:$TOKEN@$url $destdir/$project
		fi
	done
done
