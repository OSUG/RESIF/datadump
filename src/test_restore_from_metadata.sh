# Ce script doit tester les archives stockées sur le serveur iRODS du CCIN2P3
# Prérequis :
# 1. Accès au serveur irods
# 2. Accès à l'archive SUMMER (TODO: automontage bynet ?)
# 3. exécutable msi disponible

# Déroulement du test
# - soit on donne une clé en paramètre du script (Ex. FR_FILF_2015)
# - soit une clé est générée aléatoirement à partir des métadonnées publiées par le webservice station
# - on teste si on a de la donnée locale pour cette clé
#   si on n'en a pas, ça veut dire que l'archive a été détruite ou déplacée ... bref, aucun moyen de faire les tests suivants, on sort
# - on demande à irods des infos sur la donnée correspondant à la clé (ils -L)
#   s'il n'y en a pas, c'est que les dumps n'ont pas été réalisés => ERROR
# - on récupère le dump distant et on en extrait un fichier au hasard
# - on compare les hash MD5 du fichier extrait et de la version actuelle
# - on compare les hash MD5 de la sortie des commandes msi sur les 2 fichiers.
workdir=/scratch/resif_datadump/test_restore
archivedir=/osug-dc/resif/validated_seismic_data

function test_random_file {
    echo === Extracting one random file ===
    target_line=$(tar tvf latest.tar | grep -e '[0-9]$' | shuf -n 1)
    target_file=$(echo "$target_line"| awk '{print $NF}') 
    echo $target_file
    tar -xf latest.tar $target_file
    target_file_date=$(date +%s -d $(stat $target_file --print %z | awk '{print $1}'))
    fname=$(echo $target_file | awk -F'/' '{print $NF}')
    echo $fname
    regex="([A-Z0-9]+)\.([A-Z]+)\.[A-Z0-9]*\.([A-Z0-9]{3}\.D)\.([12][0-9]{3})\.[0-3][0-9]{2}"
    archive_file=""
    if [[ $fname =~ $regex ]]; then
        archive_file="$archivedir/${BASH_REMATCH[4]}/${BASH_REMATCH[1]}/${BASH_REMATCH[2]}/${BASH_REMATCH[3]}/$fname";
        if [[ ! -r $archive_file ]]; then
            printf "\033[1;33mWARNING\033[0m $archive_file not preset\n"
            exit 1
        fi
    fi
    echo $archive_file
    archive_file_date=$(date +%s -d $(stat $archive_file --print %z | awk '{print $1}'))

    if [ $archive_file_date -gt $target_file_date ]; then
        echo "File in archive is newer than remote file. Skipping md5 test"
    else
        echo === Checking md5 ===
        dumped_md5=$(md5sum $target_file | awk '{print $1}')
        data_md5=$(md5sum $archive_file | awk '{print $1}')
        echo $dumped_md5 $target_file
        echo $data_md5 $archive_file
        if [[ $dumped_md5 == $data_md5 ]]; then
            printf "\033[0;32mOK\033[0m $target_file md5 sum is correct\n"
        else
            printf "\033[0;31mError\033[0m Files $target_file and $archive_file mismatch\n"
            exit 1
        fi
    fi  

    echo === Compare msi traces ===
    dumped_msi=$(msi $target_file)
    data_msi=$(msi $archive_file)

    if [[ $(md5sum <<< $dumped_msi | awk '{print $1}') == $(md5sum <<< $data_msi | awk '{print $1}') ]]; then
        printf "\033[0;32mOK\033[0m Traces match\n"
    else
        echo "Traces of remote file $target_file"
        echo "$dumped_msi"
        echo "Traces of local file $archive_file"
        echo "$data_msi"
        printf "\033[1;33mWARNING\033[0m Traces mismatch\n"
    fi
}

if [[ $# -eq 0 ]]; then
    # Pas de clé en paramètre, récupère un channel au hasard dans les métadonnées
    line=$(wget -q -O - "http://ws.resif.fr/fdsnws/station/1/query?level=channel&format=text" | shuf -n 1)
    echo $line
    [[ $line =~ ^(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|(.*)\|([1-2][0-9][0-9][0-9]).*\|([1-2][0-9][0-9][0-9]).*$ ]]
    net=${BASH_REMATCH[1]}
    sta=${BASH_REMATCH[2]}
    start=${BASH_REMATCH[16]}
    end=${BASH_REMATCH[17]}
    echo "$start $end"
    if [[ $start -eq $end ]]; then 
        year=$start
    else
            if [[ "$end" = "2500" ]]; then
            # Les canaux permanents finissenten 2500, on ramène à l'année de 6 mois plus tôt (le dernier dump)
            end=$(date +%Y -d '6 months ago')
        fi
        year=$(( ( RANDOM % (( $end - $start )) ) + $start ))
    fi
    key="${year}_${net}_${sta}"
else
    key=$1
fi 
# On teste pour vérifier que cette donnée existe (c'est pas forcé)
dir_to_test=$(echo $key | tr '_' '/')
stat $archivedir/$dir_to_test
if [[ $? -ne 0 ]]; then
    printf "\033[1;33mWARNING\033[0m no data for this channel on archive\n"
    exit 0
fi
workdir=$workdir/$key
mkdir -p $workdir
cd $workdir

echo === Trying to retrieve $key ===
ils -L $key/latest.tar
if [[ $? -ne 0 ]]; then
    printf "\033[0;31mError\033[0m Remote file not found.\n"
    exit 1
else
    printf "\033[0;32mOK\033[0m\n"
fi

echo === Retrieving $key ===
iget $key/latest.tar
if [[ $? -ne 0 ]]; then
    printf "\033[0;31mError\033[0m iget command failed\n"
    exit 1
else
    printf "\033[0;32mOK\033[0m\n"
fi

nbtests=$(( $(tar -tf latest.tar | wc -l) / 10 ))
for i in $(seq $nbtests); do
    test_random_file
done

